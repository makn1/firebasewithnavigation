import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import LogInScreen from '../Screens/loginScreen';
import UserDetails from '../Screens/userDetailsScreen';
import MessagesScreen from '../Screens/messagesScreen';
const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

//Creating the Home screen that will contain UserDetails and messages as Tabs
//Next step would be too add some awesome icons for the tabs

function Home() {
  return (
    <Tab.Navigator>
      <Tab.Screen name="UserDetails" component={UserDetails} />
      <Tab.Screen name="Messages" component={MessagesScreen} />
    </Tab.Navigator>
  );
}

//Putting the appNavigator together with nesting navigator
//For login screen to show there need to be a route that point to it. Like a button or a auth check
//HeaderShown is false because we dont want too see "Home" in header on all screens
export default function AppNavigator() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={Home}
          options={{headerShown: false}}
        />
        <Stack.Screen name="LogInScreen" component={LogInScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
