import React from 'react';
import {Text, View} from 'react-native';

function MessagesScreen() {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Messages</Text>
    </View>
  );
}

export default MessagesScreen;
