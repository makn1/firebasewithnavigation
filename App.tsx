import React from 'react';
import AppNavigator from './App/Navigation/appNavigator';

const App = () => {
  return <AppNavigator />;
};

export default App;
